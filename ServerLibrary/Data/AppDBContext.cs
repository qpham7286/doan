﻿
using BaseLibrary.Entities;
using Microsoft.EntityFrameworkCore;

namespace ServerLibrary.Data
{
    public class AppDBContext(DbContextOptions<AppDBContext> options) : DbContext(options)
    {
        public DbSet<Employee> Employees { get; set; }
        // Genaral Departments/Department and Branch
        public DbSet<GeneralDepartment> GeneralDepartments { get; set; }

        public DbSet<Department> Departments { get; set; }

        public DbSet<Branch> Branches { get; set; }
        // Country/ City and Town
        public DbSet<Town> Towns { get; set; }
        //Authentication /Role/ SystemRole

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<SystemRole> SystemRoles { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<RefreshTokenInfo> RefreshTokenInfos { get; set; }
        //Other/ Vacation / Sanction/ Doctor /Overtime
        public DbSet<Vacation> Vacations { get; set; }

        public DbSet<VacationType> VacationTypes { get; set; }

        public DbSet<Overtime> Overtimes { get; set; }

        public DbSet<OvertimeType> OvertimeTypes { get; set; }

        public DbSet<Sanction> Sanctions { get; set; }

        public DbSet<SanctionType> SanctionTypes { get; set; }

        public DbSet<Doctor> Doctors { get; set; }
    }
}

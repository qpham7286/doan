﻿

namespace BaseLibrary.Entities
{
    public class Department : BaseEntity
    {
        public GeneralDepartment? GeneralDepartment { set; get; }
        public int GeneralDepartmentId { get; set; }
        public List<Branch>? Branches { get; set; }
    }
}

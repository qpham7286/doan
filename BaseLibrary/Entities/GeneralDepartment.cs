﻿
namespace BaseLibrary.Entities
{
    public class GeneralDepartment: BaseEntity
    {
        public List<Department>? Departments{ set; get; }
    }
}

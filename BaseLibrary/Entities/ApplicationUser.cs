﻿

namespace BaseLibrary.Entities
{
    public class ApplicationUser
    {
        public int Id { get; set; }
        public string? Fullname { get; set; }
        public String? Email { get; set; }
        public string? Password { get; set; }
    }
}

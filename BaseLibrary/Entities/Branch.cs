﻿

namespace BaseLibrary.Entities
{
    public class Branch: BaseEntity
    {
        public Department? Department { set; get; }
        public int DepartmentId { get; set; }
        public List<Employee>? Employees { get; set; }
    }
}

﻿
using BaseLibrary.DTOs;
using BaseLibrary.Reponses;

namespace ClientLibrary.Services.Contracts
{
    public interface IUserAccountServices
    {
        Task<GeneralResponse> CreateAsync(Register user);

        Task<LoginResponse> SignInAsync(Login user);

        Task<LoginResponse> RefreshTokenAsync(RefreshToken token);

        Task<WeatherForecast[]> GetWeatherForecast();
    }
}
